$(function(){
	
	var imp = impress();
	
	$('#arrowLeft').click(function(e){
		imp.prev();
		e.preventDefault();
	});
	
	$('#arrowRight').click(function(e){
		imp.next();
		e.preventDefault();
	});

	$('#click-1').click(function(e){

		imp.goto(document.getElementById( 'music' ));
		e.preventDefault();
	});
});
